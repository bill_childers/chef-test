#
# Cookbook Name:: nginx
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.
#
package 'epel-release'

package 'nginx'

directory '/usr/share/nginx/html' do
  recursive true
end

template '/usr/share/nginx/html/index.html' do
  source 'index.html.erb'
  mode '0644'
end

template '/etc/nginx/nginx.conf' do
  notifies :reload, 'service[nginx]', :immediately
end

directory '/etc/nginx/sites-enabled' do
  recursive true
end

template '/etc/nginx/sites-enabled/default.conf' do
  notifies :reload, 'service[nginx]', :immediately
end

service 'nginx' do
  supports status: true, restart: true, reload: true
  action [:enable, :start]
end
